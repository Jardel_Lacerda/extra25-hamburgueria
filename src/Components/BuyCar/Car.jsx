import { Component } from "react";
import "./styleCar.css";

export default class Car extends Component {
  render() {
    let { saleDetails } = this.props.currentSale;
    return saleDetails.map((item) => {
      return (
        <div className="card">
          <h3>{item.name}</h3>
          <h5>Category - {item.category}</h5>
          <h5>Preço - {item.price}</h5>
        </div>
      );
    });
  }
}
