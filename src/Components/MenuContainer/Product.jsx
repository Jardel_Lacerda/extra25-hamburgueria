import { Component } from "react";
import "./styleContainer.css";

export default class Product extends Component {
  render() {
    const { name, category, price } = this.props.item;
    return (
      <div className="divStyle">
        <h2>{name}</h2>
        <p>Categoria - {category}</p>
        <p>Preço - {price} R$</p>
        <button onClick={() => this.props.action(this.props.item)}>
          ATUALIZAR
        </button>
      </div>
    );
  }
}
