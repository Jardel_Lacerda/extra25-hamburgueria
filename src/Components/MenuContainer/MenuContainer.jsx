import { Component } from "react";
import Product from "./Product";

export default class MenuContainer extends Component {
  render() {
    let local = this.props.filter > 0 ? this.props.filter : this.props.product;
    return local.map((item, index) => (
      <Product action={this.props.action} key={index} item={item} />
    ));
  }
}
