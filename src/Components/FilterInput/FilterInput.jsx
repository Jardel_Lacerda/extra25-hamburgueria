import { Component } from "react";
import "./styleFilter.css";

export default class Input extends Component {
  render() {
    return (
      <div className="div">
        <input id="escolha" name="escolha" placeholder="escolha"></input>

        <button onClick={this.props.action} id="bt">
          FILTRAR
        </button>
      </div>
    );
  }
}
