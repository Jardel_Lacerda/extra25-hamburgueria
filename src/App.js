import { Component } from "react";
import "./App.css";
import Car from "./Components/BuyCar/Car";
import Input from "./Components/FilterInput/FilterInput";
import MenuContainer from "./Components/MenuContainer/MenuContainer";

export default class App extends Component {
  state = {
    products: [
      { id: 1, name: "Hamburguer", category: "Sanduíches", price: 7.99 },
      { id: 2, name: "X-Burguer", category: "Sanduíches", price: 8.99 },
      { id: 3, name: "X-Salada", category: "Sanduíches", price: 10.99 },
      { id: 4, name: "Big Kenzie", category: "Sanduíches", price: 16.99 },
      { id: 5, name: "Guaraná", category: "Bebidas", price: 4.99 },
      { id: 6, name: "Coca", category: "Bebidas", price: 4.99 },
      { id: 7, name: "Fanta", category: "Bebidas", price: 4.99 },
      { id: 8, name: "Coca-Cola", category: "Bebidas", price: 5.21 },
    ],
    filteredProducts: [],
    currentSale: { total: 0, saleDetails: [] },
  };

  showProducts = () => {
    let value = document.getElementById("escolha").value;
    console.log(value);
    if (value !== "") {
      this.setState({
        filteredProducts: [
          this.state.products.find((item) => {
            return item.name === value;
          }),
        ],
      });
    } else {
      this.setState({ filteredProducts: this.state.products });
    }

    console.log(this.state.filteredProducts);
  };

  handleClick = (productId) => {
    let result = [productId].reduce((total, currentValue) => {
      return total + currentValue.price;
    }, 0);

    this.setState({
      currentSale: {
        total: result + this.state.currentSale.total,
        saleDetails: [...this.state.currentSale.saleDetails, productId],
      },
    });
  };

  render() {
    return (
      <section>
        <div className="selecters">
          <Input action={this.showProducts} />
        </div>

        {this.state.filteredProducts.length > 0 ? (
          <div className="container">
            <MenuContainer
              action={this.handleClick}
              product={this.state.filteredProducts}
            />
          </div>
        ) : (
          <div className="container">
            <MenuContainer
              action={this.handleClick}
              product={this.state.products}
            />
          </div>
        )}

        <h3
          style={{
            margin: "50px",
          }}
        >
          Total:
          {this.state.currentSale.total.toLocaleString("pt-BR", {
            style: "currency",
            currency: "BRL",
          })}
        </h3>

        <div className="car">
          <Car currentSale={this.state.currentSale} />
        </div>
      </section>
    );
  }
}
